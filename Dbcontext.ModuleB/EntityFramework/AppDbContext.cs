﻿using Dbcontext.ModuleA.Domain;
using Dbcontext.ModuleB.Domain;
using Microsoft.EntityFrameworkCore;

namespace Dbcontext.ModuleB.EntityFramework
{
    public class AppDbContextB : DbContext
    {

        public AppDbContextB( DbContextOptions<AppDbContextB> options) : base(options)
        {
        }

        public DbSet<Solution> Solutions { get; set; }
        
    }
}