﻿using Dbcontext.ModuleB.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dbcontext.ModuleB
{
    public static class ServicesConfigurator
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContextB>(options =>
                options.UseNpgsql(configuration.GetConnectionString("ConnectionStringB"), migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__BMigrationsHistory", "ModuleB");
                    migrationOptions.MigrationsAssembly("Dbcontext.Web");
                }));
        }
    }
}