﻿using System.ComponentModel.DataAnnotations.Schema;
using Dbcontext.ModuleA.Domain;

namespace Dbcontext.ModuleB.Domain
{
    [Table("Solutions", Schema = "ModuleB")]
    public class Solution
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public int OwnerId { get; set; }
    }
}