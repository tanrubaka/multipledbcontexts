using System;
using System.Linq;
using System.Transactions;
using Dbcontext.ModuleA.Domain;
using Dbcontext.ModuleA.EntityFramework;
using Dbcontext.ModuleB.Domain;
using Dbcontext.ModuleB.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Xunit;

namespace Dbcontext.Test
{
    public class UnitTest1
    {
        private readonly string _connectionString =
            "User ID=postgres;Password=123;Host=localhost;Port=5432;Database=ModuleTest;Pooling=true;";
        [Fact]
        public void Test1()
        {
            var builder = new DbContextOptionsBuilder<AppDbContextA>()
                .UseNpgsql(_connectionString, migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__AMigrationsHistory", "ModuleA");
                });

            var context = new AppDbContextA(builder.Options);
            context.Users.Add(new User());
            context.SaveChanges();
            Assert.True(true);
        }

        [Fact]
        public void Test2()
        {
            var builder = new DbContextOptionsBuilder<AppDbContextA>()
                .UseNpgsql(_connectionString, migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__AMigrationsHistory", "ModuleA");
                });


            var builderB = new DbContextOptionsBuilder<AppDbContextB>()
                .UseNpgsql(_connectionString, migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__BMigrationsHistory", "ModuleB");
                });


            using (var scope = new TransactionScope())
            {
                var user = new User();

                using (var contextA = new AppDbContextA(builder.Options))
                {
                    contextA.Users.Add(user);
                    contextA.SaveChanges();
                }

                using (var contextB = new AppDbContextB(builderB.Options))
                {
                    contextB.Solutions.Add(new Solution { Name = "test", OwnerId = user.Id });
                    contextB.Solutions.Add(new Solution { Name = "test2", OwnerId = user.Id });
                    contextB.SaveChanges();
                }

                scope.Complete();
            }

            Assert.True(true);
        }

        [Fact]
        public void Test3()
        {
            var builder = new DbContextOptionsBuilder<AppDbContextA>()
                .UseNpgsql(_connectionString, migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__AMigrationsHistory", "ModuleA");
                });


            var builderB = new DbContextOptionsBuilder<AppDbContextB>()
                .UseNpgsql(_connectionString, migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__BMigrationsHistory", "ModuleB");
                });

            var contextA = new AppDbContextA(builder.Options);
            var contextB = new AppDbContextB(builderB.Options);

            //var users = contextA.Users.AsQueryable().Select(o => o.Id);
            //var list2 = contextB.Solutions
            //    .Join(users, s => s.OwnerId, u => u, (s, u) => new { s.Id, s.Name, s.OwnerId, UserId = u }).ToList();

            //var sql = ((System.Data.Entity.Core.Objects.ObjectQuery)users)
            //    .ToTraceString();
            var list = contextB.Solutions
                .Join(contextB.Solutions.FromSql("SELECT \"Id\" FROM \"ModuleA\".\"Users\""), s => s.OwnerId, u => u.Id, (s, u) => new {s.Id, s.Name, s.OwnerId, UserId = u.Id}).ToList();

            Assert.True(true);
        }
    }
}
