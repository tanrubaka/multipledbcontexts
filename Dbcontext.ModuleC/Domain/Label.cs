﻿using System.ComponentModel.DataAnnotations.Schema;
using Dbcontext.ModuleA.Domain;

namespace Dbcontext.ModuleC.Domain
{
    [Table("Labels", Schema = "ModuleC")]
    public class Label
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public User Owner { get; set; }
    }
}