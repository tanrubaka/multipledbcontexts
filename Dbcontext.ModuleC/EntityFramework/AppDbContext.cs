﻿using Dbcontext.ModuleA.Domain;
using Dbcontext.ModuleC.Domain;
using Microsoft.EntityFrameworkCore;

namespace Dbcontext.ModuleC.EntityFramework
{
    public class AppDbContextC : DbContext
    {
        public AppDbContextC(DbContextOptions<AppDbContextC> options) : base(options)
        {

        }
        
        public DbSet<Label> Labels { get; set; }
    }
}