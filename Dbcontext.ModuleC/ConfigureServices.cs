﻿
using Dbcontext.ModuleC.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dbcontext.ModuleC
{
    public static class ServicesConfigurator
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContextC>(options =>
                options.UseNpgsql(configuration.GetConnectionString("ConnectionStringC"), migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__CMigrationsHistory", "ModuleC");
                    migrationOptions.MigrationsAssembly("Dbcontext.Web");
                }));
        }
    }
}