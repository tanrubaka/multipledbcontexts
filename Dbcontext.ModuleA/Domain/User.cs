﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Dbcontext.ModuleA.Domain
{
    [Table("Users", Schema = "ModuleA")]
    public class User
    {
        public int Id { get; set; }
    }
}