﻿using Dbcontext.ModuleA.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Dbcontext.ModuleA.EntityFramework
{
    public class AppDbContextA:DbContext
    {
        public AppDbContextA(DbContextOptions<AppDbContextA> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}