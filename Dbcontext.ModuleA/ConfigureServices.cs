﻿using Dbcontext.ModuleA.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dbcontext.ModuleA
{
    public static class ServicesConfigurator
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContextA>(options =>
                options.UseNpgsql(configuration.GetConnectionString("ConnectionStringA")
                    , migrationOptions =>
                {
                    migrationOptions.MigrationsHistoryTable("__AMigrationsHistory", "ModuleA");
                    migrationOptions.MigrationsAssembly("Dbcontext.Web");
                }));
        }
    }
}